package ru.apolyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;

import static ru.apolyakov.tm.util.TerminalUtil.readLine;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @NotNull
    private final static String NAME = "task-start-by-id";

    @NotNull
    private final static String DESCRIPTION = "Starting task by ID";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String id = readLine(ID_INPUT);
        throwExceptionIfNull(getTaskService().startById(id));
    }

}
