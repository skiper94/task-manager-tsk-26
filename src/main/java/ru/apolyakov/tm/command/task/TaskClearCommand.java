package ru.apolyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;

import static ru.apolyakov.tm.util.TerminalUtil.printLinesWithEmptyLine;

public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    private final static String NAME = "tasks-clear";

    @NotNull
    private final static String DESCRIPTION = "deleting all tasks";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        final int count = getTaskService().getSize();
        getTaskService().clear();
        printLinesWithEmptyLine(count + " tasks removed.");
    }

}
