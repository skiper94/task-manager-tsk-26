package ru.apolyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.util.TerminalUtil;

public final class ProjectCompleteByNameCommand extends AbstractProjectCommand {

    @NotNull
    private final static String NAME = "project-finish-by-name";

    @NotNull
    private final static String DESCRIPTION = "Finishing project by name";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String name = TerminalUtil.readLine(NAME_INPUT);
        throwExceptionIfNull(getProjectService().completeByName(name));
    }

}
