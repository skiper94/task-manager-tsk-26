package ru.apolyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private final static String NAME = "project-remove-by-index";

    @NotNull
    private final static String DESCRIPTION = "Removing project by index";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        final int index = TerminalUtil.readNumber(INDEX_INPUT);
        throwExceptionIfNull(getProjectTaskService().removeProjectByIndex(index - 1));
    }

}
