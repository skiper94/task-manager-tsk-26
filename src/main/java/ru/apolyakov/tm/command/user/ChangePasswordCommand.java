package ru.apolyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.exception.security.AccessDeniedNotAuthorizedException;
import ru.apolyakov.tm.model.User;

import java.util.Optional;

import static ru.apolyakov.tm.util.TerminalUtil.readLine;

public final class ChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "password-change";

    @NotNull
    private static final String DESCRIPTION = "Changing user's password";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String userId = Optional.ofNullable(getAuthService().getUserId())
                .orElseThrow(AccessDeniedNotAuthorizedException::new);
        @Nullable final User user = getUserService().findOneById(userId);
        Optional.ofNullable(user).orElseThrow(AccessDeniedNotAuthorizedException::new);
        @Nullable final String login;
        if (getAuthService().isPrivilegedUser()) login = readLine(ENTER_LOGIN);
        else login = user.getLogin();
        @NotNull final String password = readLine(ENTER_PASSWORD);
        getUserService().setPassword(login, password);
    }

}
