package ru.apolyakov.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.exception.AbstractException;

public final class EmailEmptyException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Error! Email is empty.";

    public EmailEmptyException() {
        super(MESSAGE);
    }

}
