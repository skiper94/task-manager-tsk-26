package ru.apolyakov.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.exception.AbstractException;

public final class CommandNameEmptyException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Error! Command name cannot be empty.";

    public CommandNameEmptyException() {
        super(MESSAGE);
    }
}
