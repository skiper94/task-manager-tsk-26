package ru.apolyakov.tm.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.exception.AbstractException;

public final class UserExistsWIthEmailException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "User already exists with email: ";

    public UserExistsWIthEmailException(final String email) {
        super(MESSAGE + email);
    }

}
