package ru.apolyakov.tm.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.exception.AbstractException;

public final class TaskNotFoundException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Task not found.";

    public TaskNotFoundException() {
        super(MESSAGE);
    }

}
