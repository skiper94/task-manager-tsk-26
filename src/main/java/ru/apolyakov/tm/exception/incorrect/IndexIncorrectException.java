package ru.apolyakov.tm.exception.incorrect;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.exception.AbstractException;

public final class IndexIncorrectException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Error! Incorrect index: ";

    public IndexIncorrectException(final int index) {
        super(MESSAGE + index);
    }

}
