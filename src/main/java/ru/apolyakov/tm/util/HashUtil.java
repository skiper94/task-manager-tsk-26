package ru.apolyakov.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.api.property.IProperty;

import static ru.apolyakov.tm.util.ValidationUtil.isEmptyStr;

@UtilityClass
public final class HashUtil {

    @Nullable
    public static String md5(@NotNull final String value) {
        if (isEmptyStr(value)) return null;
        try {
            @NotNull final java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            @NotNull byte[] array = md.digest(value.getBytes());
            @NotNull final StringBuilder sb = new StringBuilder();
            for (final byte b : array) {
                sb.append(Integer.toHexString((b & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString();
        } catch (@NotNull java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Nullable
    public static String salt(@Nullable final String value, @NotNull final IProperty passwordProperty) {
        final int iteration = passwordProperty.getPasswordIteration();
        @NotNull final String secret = passwordProperty.getPasswordSecret();
        if (isEmptyStr(value)) return null;
        @Nullable String result = value;
        for (int i = 0; i < iteration; i++) result = md5(secret + result + secret);
        return result;
    }
}
