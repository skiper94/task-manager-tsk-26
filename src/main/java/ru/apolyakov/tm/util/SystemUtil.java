package ru.apolyakov.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

@UtilityClass
public final class SystemUtil {

    public static long getPID() {
        @NotNull final String processName = java.lang.management.ManagementFactory.getRuntimeMXBean().getName();
        if (processName != null && processName.length() > 0) {
            try {
                return Long.parseLong(processName.split("@")[0]);
            }
            catch (@NotNull final Exception e) {
                return 0;
            }
        }
        return 0;
    }

}
