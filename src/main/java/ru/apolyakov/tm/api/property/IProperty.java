package ru.apolyakov.tm.api.property;

import org.jetbrains.annotations.NotNull;

public interface IProperty {

    @NotNull String getAppVer();

    @NotNull String getPasswordSecret();

    @NotNull Integer getPasswordIteration();

}
