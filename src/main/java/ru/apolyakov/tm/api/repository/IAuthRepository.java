package ru.apolyakov.tm.api.repository;

import org.jetbrains.annotations.Nullable;

public interface IAuthRepository {

    @Nullable String getUserId();

    void setUserId(@Nullable String currentUserId);

}
