package ru.apolyakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IBusinessEntityService<Task> {

    @Nullable
    Task addTaskToProject(@NotNull String taskId, @NotNull String projectId);

    @NotNull
    List<Task> findAllByProjectId(@NotNull String projectId, @NotNull Comparator<Task> comparator);

    void removeAllByProjectId(String projectId);

    @Nullable
    Task removeTaskFromProject(@NotNull String taskId, @NotNull String projectId);

}
