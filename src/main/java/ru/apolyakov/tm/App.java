package ru.apolyakov.tm;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.bootstrap.Bootstrap;

import static ru.apolyakov.tm.util.SystemUtil.getPID;


public class App {
    public static void main(String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }
}
