package ru.apolyakov.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.apolyakov.tm.api.ServiceLocator;
import ru.apolyakov.tm.api.repository.*;
import ru.apolyakov.tm.api.service.*;
import ru.apolyakov.tm.command.AbstractCommand;
import ru.apolyakov.tm.comparator.CommandComparator;
import ru.apolyakov.tm.enumerated.Role;
import ru.apolyakov.tm.exception.AbstractException;
import ru.apolyakov.tm.exception.empty.CommandDescriptionEmptyException;
import ru.apolyakov.tm.exception.empty.CommandNameEmptyException;
import ru.apolyakov.tm.exception.entity.CommandNotFoundException;
import ru.apolyakov.tm.repository.*;
import ru.apolyakov.tm.service.*;
import ru.apolyakov.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.apolyakov.tm.constant.StringConst.*;
import static ru.apolyakov.tm.util.SystemUtil.getPID;
import static ru.apolyakov.tm.util.TerminalUtil.printConfirmCommand;
import static ru.apolyakov.tm.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.apolyakov.tm.util.ValidationUtil.isEmptyStr;

@Getter
public final class Bootstrap implements ServiceLocator {

    private static final boolean CONSOLE_LOG_ENABLED = true;

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ILogService logService = new LogService(CONSOLE_LOG_ENABLED);

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthRepository authRepository = new AuthRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IAuthService authService = new AuthService(authRepository, userService, propertyService);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository, authService);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository, authService);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskService, projectService, authService);


    public void executeCommand(@NotNull final String commandName) {
        if (isEmptyStr(commandName)) return;
        @NotNull final AbstractCommand command = Optional.ofNullable(commandService.getCommands().get(commandName))
                .orElseThrow(() -> new CommandNotFoundException(commandName));
        printConfirmCommand(command.getDescription());
        execute(command);
    }

    public void executeCommandByArgument(@NotNull final String argument) {
        @NotNull final AbstractCommand command = Optional.ofNullable(commandService.getArguments().get(argument))
                .orElseThrow(() -> new CommandNotFoundException(argument));
        execute(command);
    }

    public void execute(@NotNull final AbstractCommand command) {
        if (command.getNeedAuthorization()) authService.throwExceptionIfNotAuthorized();
        authService.checkRoles(command.getRoles());
        command.execute();
    }

    @SneakyThrows
    public void initCommands() {
        @NotNull final Reflections reflections = new Reflections("ru.apolyakov.tm.command");
        @NotNull final List<Class<? extends AbstractCommand>> classes = reflections
                .getSubTypesOf(ru.apolyakov.tm.command.AbstractCommand.class)
                .stream()
                .sorted(CommandComparator.getInstance())
                .collect(Collectors.toList());
        boolean isAbstract;
        for (@NotNull final Class<? extends AbstractCommand> classCommand : classes) {
            isAbstract = Modifier.isAbstract(classCommand.getModifiers());
            if (isAbstract) continue;
            registerCommand(classCommand.newInstance());
        }
    }

    private void initData() {
        try {
            userService.add("admin", "admin", "apolyakov@tsconsalting.com", Role.ADMIN, "Andrey", "Dmitrievich", "Polyakov");
            userService.add("user", "user", "user@tsconsalting.com", Role.USER, "Ivan", "Ivanovich", "Ivanov");

            authService.login("admin", "admin");

            taskService.add("Task #1", "Description of task #1");
            taskService.add("Task #2", "Description of task #2");
            taskService.add("Task #3", "Description of task #3");
            taskService.add("Task #4", "Description of task #4");
            taskService.add("Task #5", "Description of task #5");
            taskService.startByName("Task #2");
            taskService.completeByName("Task #3");

            projectService.add("Project #1", "Description of Project #1");
            projectService.add("Project #2", "Description of Project #2");
            projectService.add("Project #3", "Description of Project #3");
            projectService.add("Project #4", "Description of Project #4");
            projectService.add("Project #5", "Description of Project #5");

            projectTaskService.addTaskToProject(Objects.requireNonNull(projectService.findOneByName("Project #4")).getId(),
                    Objects.requireNonNull(taskService.findOneByName("Task #4")).getId());

            authService.logout();
            authService.login("user", "user");

            taskService.add("Task #6", "Description of task #6");
            taskService.add("Task #7", "Description of task #7");

            projectService.add("Project #6", "Description of Project #6");
            projectService.add("Project #7", "Description of Project #7");

            projectTaskService.addTaskToProject(Objects.requireNonNull(projectService.findOneByName("Project #6")).getId(),
                    Objects.requireNonNull(taskService.findOneByName("Task #7")).getId());

            authService.logout();

        } catch (@NotNull final AbstractException e) {
            logService.error(e);
        }
    }

    @SneakyThrows
    public void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(getPID());
        Files.write(Paths.get("task-manager.pid"), pid.getBytes(StandardCharsets.UTF_8));
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    @SuppressWarnings("InfiniteLoopStatement")
    public void processInput() {
        logService.info("Program run in command-line mode.");
        initPID();
        initData();
        showWelcome();
        @NotNull String command = readCommand();
        while (true) {
            try {
                if (isEmptyStr(command)) {
                    command = readCommand();
                    continue;
                }
                logService.command("Executing command: " + command);
                executeCommand(command);
                printConfirmCommand(APP_COMMAND_SUCCESS);
            } catch (@NotNull final AbstractException e) {
                logService.error(e);
                printConfirmCommand(APP_COMMAND_ERROR);
            }
            command = readCommand();
        }
    }

    @NotNull
    public String readCommand() {
        return TerminalUtil.readLine(COMMAND_PROMPT);
    }

    public void registerCommand(@NotNull final AbstractCommand command) {
        try {
            @NotNull final String terminalCommand = command.getCommand();
            @NotNull final String commandDescription = command.getDescription();
            @Nullable final String commandArgument = command.getArgument();
            if (isEmptyStr(terminalCommand)) throw new CommandNameEmptyException();
            if (isEmptyStr(commandDescription)) throw new CommandDescriptionEmptyException();
            command.setServiceLocator(this);
            commandService.getCommands().put(terminalCommand, command);
            if (isEmptyStr(commandArgument)) return;
            commandService.getArguments().put(commandArgument, command);
        } catch (@NotNull final AbstractException e) {
            logService.error(e);
        }
    }

    public void run(@NotNull final String... args) {
        initCommands();
        if (args.length == 0) processInput();
        else
            try {
                logService.info("Program run in arguments mode.");
                executeCommandByArgument(args[0]);
            } catch (@NotNull final AbstractException e) {
                logService.error(e);
            }
    }

    private void showWelcome() {
        printLinesWithEmptyLine(APP_WELCOME_TEXT);
    }

}
