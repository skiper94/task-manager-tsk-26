package ru.apolyakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.api.repository.IBusinessEntityRepository;
import ru.apolyakov.tm.enumerated.Status;
import ru.apolyakov.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class AbstractBusinessEntityRepository<E extends AbstractBusinessEntity> extends AbstractModelRepository<E> implements IBusinessEntityRepository<E> {

    @NotNull
    @Override
    public List<E> findAll(@NotNull final Comparator<E> comparator) {
        return entities.values().stream().sorted(comparator).collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<E> findAllLimitByUser(@NotNull final String userId) {
        return entities.values().stream()
                .filter(predicateByUser(userId))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<E> findAllLimitByUser(@NotNull final String userId, @NotNull final Comparator<E> comparator) {
        return entities.values().stream()
                .filter(predicateByUser(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public E findOneByIdLimitByUser(@NotNull final String userId, @NotNull final String id) {
        final Optional<E> entity = Optional.ofNullable(findOneById(id));
        return entity.filter(predicateByUser(userId)).orElse(null);
    }

    @Nullable
    @Override
    public E findOneByIndex(final int index) {
        return entities.values().stream()
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public E findOneByIndexLimitByUser(@NotNull final String userId, final int index) {
        return entities.values().stream()
                .filter(predicateByUser(userId))
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public E findOneByName(@NotNull final String name) {
        return entities.values().stream()
                .filter(predicateByName(name))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public E findOneByNameLimitByUser(@NotNull final String userId, @NotNull final String name) {
        return entities.values().stream()
                .filter(predicateByUser(userId))
                .filter(predicateByName(name))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public E removeOneByIndex(final int index) {
        @Nullable final E entity = findOneByIndex(index);
        Optional.ofNullable(entity).ifPresent(this::remove);
        return entity;
    }

    @Nullable
    @Override
    public E removeOneByIndexLimitByUser(@NotNull final String userId, final int index) {
        @Nullable final E entity = findOneByIndexLimitByUser(userId, index);
        Optional.ofNullable(entity).ifPresent(this::remove);
        return entity;
    }

    @Nullable
    @Override
    public E removeOneByName(@NotNull final String name) {
        @Nullable final E entity = findOneByName(name);
        Optional.ofNullable(entity).ifPresent(this::remove);
        return entity;
    }

    @Nullable
    @Override
    public E removeOneByNameLimitByUser(@NotNull final String userId, @NotNull final String name) {
        @Nullable final E entity = findOneByNameLimitByUser(userId, name);
        Optional.ofNullable(entity).ifPresent(this::remove);
        return entity;
    }

    @Override
    public void clear() {
        entities.clear();
    }

    @Override
    public void clearLimitByUser(@NotNull final String userId) {
        findKeysLimitByUser(userId).forEach(entities::remove);
    }

    @Nullable
    @Override
    public E update(@NotNull final String id, @NotNull final String name, @Nullable final String description) {
        final E entity = findOneById(id);
        return updateEntity(entity, name, description).orElse(null);
    }

    @NotNull
    private Optional<E> updateEntity(@Nullable final E entity, @NotNull final String name, @Nullable final String description) {
        @NotNull final Optional<E> optional = Optional.ofNullable(entity);
        optional.ifPresent(e -> {
            e.setName(name);
            e.setDescription(description);
        });
        return optional;
    }

    @Nullable
    @Override
    public E updateOneLimitByUser(@NotNull final String userId, @NotNull final String id, @NotNull final String name, @Nullable final String description) {
        @Nullable final E entity = (findOneByIdLimitByUser(userId, id));
        return updateEntity(entity, name, description).orElse(null);
    }

    @NotNull
    private Optional<E> updateStatus(@Nullable final E entity, @NotNull final Status status) {
        @NotNull final Optional<E> optional = Optional.ofNullable(entity);
        optional.ifPresent(e -> {
            e.setStatus(status);
            if (status.equals(Status.IN_PROGRESS)) e.setDateStart(new Date());
            else if (status.equals(Status.COMPLETED)) e.setDateFinish(new Date());
        });
        return optional;
    }

    @Nullable
    @Override
    public E updateStatusById(@NotNull final String id, @NotNull final Status status) {
        @Nullable final E entity = findOneById(id);
        return updateStatus(entity, status).orElse(null);
    }

    @Nullable
    @Override
    public E updateStatusByIdLimitByUser(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        @Nullable final E entity = findOneByIdLimitByUser(userId, id);
        return updateStatus(entity, status).orElse(null);
    }

    @Nullable
    @Override
    public E updateStatusByIndex(final int index, @NotNull final Status status) {
        @NotNull final Optional<String> entityId = Optional.ofNullable(getId(index));
        return entityId.map(id -> updateStatusById(id, status)).orElse(null);
    }

    @Nullable
    @Override
    public E updateStatusByIndexLimitByUser(@NotNull final String userId, final int index, @NotNull final Status status) {
        final Optional<String> entityId = Optional.ofNullable(getIdLimitByUser(userId, index));
        return entityId.map(id -> updateStatusByIdLimitByUser(userId, id, status)).orElse(null);
    }

    @Nullable
    @Override
    public E updateStatusByName(@NotNull final String name, @NotNull final Status status) {
        @NotNull final Optional<String> entityId = Optional.ofNullable(getId(name));
        return entityId.map(id -> updateStatusById(id, status)).orElse(null);
    }

    @Nullable
    @Override
    public E updateStatusByNameLimitByUser(@NotNull final String userId, @NotNull final String name, @NotNull final Status status) {
        final Optional<String> entityId = Optional.ofNullable(getIdLimitByUser(userId, name));
        return entityId.map(id -> updateStatusByIdLimitByUser(userId, id, status)).orElse(null);
    }


    @NotNull
    @Override
    public List<String> findKeysLimitByUser(@NotNull final String userId) {
        return entities.values().stream()
                .filter(predicateByUser(userId))
                .map(E::getId)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public String getId(@NotNull final String name) {
        @Nullable final E entity = findOneByName(name);
        return Optional.ofNullable(entity).isPresent() ? entity.getId() : null;
    }

    @Nullable
    @Override
    public String getId(final int index) {
        @Nullable final E entity = findOneByIndex(index);
        return Optional.ofNullable(entity).isPresent() ? entity.getId() : null;
    }

    @Nullable
    @Override
    public String getIdLimitByUser(@NotNull final String userId, final int index) {
        @Nullable final E entity = findOneByIndexLimitByUser(userId, index);
        return Optional.ofNullable(entity).isPresent() ? entity.getId() : null;
    }

    @Nullable
    @Override
    public String getIdLimitByUser(@NotNull final String userId, @NotNull final String name) {
        @Nullable final E entity = findOneByNameLimitByUser(userId, name);
        return Optional.ofNullable(entity).isPresent() ? entity.getId() : null;
    }

    @Override
    public int getSizeLimitByUser(@NotNull final String userId) {
        return findAllLimitByUser(userId).size();
    }

    @Override
    public boolean isEmptyLimitByUser(@NotNull final String userId) {
        return findAllLimitByUser(userId).isEmpty();
    }

    @Override
    public boolean isNotFoundById(@NotNull final String id) {
        return findOneById(id) == null;
    }

    @Override
    public boolean isNotFoundByIdLimitByUser(@NotNull final String userId, @NotNull final String id) {
        return findOneByIdLimitByUser(userId, id) == null;
    }

    @NotNull
    public Predicate<AbstractBusinessEntity> predicateByName(@NotNull final String name) {
        return s -> name.equals(s.getName());
    }

    @NotNull
    public Predicate<AbstractBusinessEntity> predicateByUser(@NotNull final String userId) {
        return s -> userId.equals(s.getUserId());
    }

    @Nullable
    @Override
    public E removeOneByIdLimitByUser(@NotNull final String userId, @NotNull final String id) {
        @Nullable final E entity = findOneByIdLimitByUser(userId, id);
        Optional.ofNullable(entity).ifPresent(this::remove);
        return entity;
    }

}
