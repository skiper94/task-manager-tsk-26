# TASK MANAGER

Console application for task list.

# DEVELOPER INFO

* NAME: Andrey Polyakov
* E-MAIL: apolyakov@tsconsulting.com 
* E-MAIL: adpolyakov@vtb.ru

# SOFTWARE

* JDK 15.0.1
* Windows 10 x64

# HARDWARE

* RAM 8Gb
* CPU i5
* HDD 500Gb

# BUILD PROGRAM

```
mvn clean install
```

# RUN PROCESS

```
java -jar ./task-manager.jar
```

